<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ResponseBaseController;
use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends ResponseBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::paginate(10);
        return $this->sendSuccess($suppliers, 'SUCCESS GET SUPPLIERS', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required|string',
                'email' => 'required|email',
                'telp' => 'required',
            ],
            [
                'name.required' => 'Isian Name wajib diisi',
                'email.required' => 'Isian Email wajib diisi',
                'telp.required' => 'Isian Telp wajib diisi',
                'email.email' => 'Format email salah'
            ]
        );

        try {

            $add = new Supplier();
            $add->name = $request->name;
            $add->email = $request->email;
            $add->telp = $request->telp;
            $add->phone = $request->phone;
            $add->address = $request->address;
            $add->save();

            if(!$add->save()){
                return $this->sendError('FAILED CREATED SUPPLIER',  $add, 403);    
            }
            return $this->sendSuccess($add, 'SUCCESS CREATED SUPPLIER', 201);

        } catch (\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $supplier = Supplier::findOrFail($request->id);
        return $this->sendSuccess($supplier, "SUCCESS SHOW DATA", 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        $this->validate(
            $request,
            [
                'id' => 'required',
                'name' => 'required|string',
                'email' => 'required|email',
                'telp' => 'required',
            ],
            [
                'id.required' => 'ID null',
                'name.required' => 'Isian Name wajib diisi',
                'email.required' => 'Isian Email wajib diisi',
                'telp.required' => 'Isian Telp wajib diisi',
                'email.email' => 'Format email salah'
            ]
        );

        try {

            $update = Supplier::findOrFail($request->id);    
            $update->name = $request->name;
            $update->email = $request->email;
            $update->telp = $request->telp;
            $update->phone = $request->phone;
            $update->address = $request->address;
            $update->save();

            if(!$update->save()){
                return $this->sendError('FAILED UPDATE SUPPLIER',  $update, 403);    
            }

            return $this->sendSuccess($update, 'SUCCESS UPDATE SUPPLIER', 201);

        } catch (\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {  

            $delete = Supplier::find($request->id);
            
            if(is_null($delete)){
                return $this->sendSuccess(NULL, 'NO DATA SUPPLIER', 204);
            }

            $delete->delete();

            if(!$delete){
                return $this->sendError('FAILED DELETE SUPPLIER',  $delete, 204);    
            } else {
                return $this->sendSuccess($delete, 'SUCCESS DELETED SUPPLIER', 200);
            }
            
        } catch(\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }
}
