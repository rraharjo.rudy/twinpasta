<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{

    use SoftDeletes;

    protected $table = "menus";
    
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'id_category', 'name', 'price', 'pic', 'disc', 'markup_gofood', 'markup_grabfood', 'supportojol', 'description'
    ];

    public function category()
	{
		return $this->hasOne('App\MenuCategory', 'id');
    }
}
