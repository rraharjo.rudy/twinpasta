<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;

    protected $table = "suppliers";

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email', 'telp', 'phone', 'address'
    ];
}
