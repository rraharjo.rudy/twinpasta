export const routes = [
    {
        path: '/admin/dashboard',
        name: 'dashboard',
        component: () => import('./pages/Dashboard'),
        meta: {
            Auth: true,
        }
    },
    {
        path: '/admin/category',
        name: 'category',
        component: () => import('./pages/category/Index'),
        meta: {
            Auth: true,
        },
    },
    {
        path: '/admin/stock',
        name: 'stock',
        component: () => import('./pages/stock/Index'),
        meta: {
            Auth: true,
        },
    },
    {
        path: '/admin/table',
        name: 'table',
        component: () => import('./pages/table/Index'),
        meta: {
            Auth: true,
        },
    },
    {
        path: '/admin/menu',
        name: 'menu',
        component: () => import ('./pages/menu/Index'),
        meta: {
            Auth: true,
        }
    },
    {
        path: '/admin/signin',
        name: 'signin',
        component: () => import('./pages/auth/SignIn'),
        meta: {
            Guest: true,
        }
    },
    { path: '*', redirect: '/admin/dashboard' },
]