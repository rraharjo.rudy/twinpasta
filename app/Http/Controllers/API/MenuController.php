<?php

namespace App\Http\Controllers\API;

use App\FoodSupportOnline;
use App\Menu;
use App\MenuCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends ResponseBaseController
{

    public function index_admin(){
        $data = DB::table('menus')
            ->leftJoin('menu_categories', 'menus.id_category', '=', 'menu_categories.id')
            ->select('menus.*', 'menu_categories.name as category_name')
            ->whereNull('menus.deleted_at')
            ->orderBy('menus.id', 'asc')
            ->latest()
            ->get();
            //->paginate(10);
        if(!$data){
            return $this->sendError('FAILED GET DATA MENU',  $data, 204);    
        }
        return $this->sendSuccess($data, 'SUCCESS GET DATA MENU', 200);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = array();

        $menuKategori = MenuCategory::get();

        foreach ($menuKategori as $value) { 

            $Menus = DB::table('menus')
                ->leftJoin('menu_categories', 'menus.id_category', '=', 'menu_categories.id')
                ->select('menus.*', 'menu_categories.name as category_name')
                ->whereNull('menus.deleted_at')
                ->where('menus.id_category', '=', $value->id)
                ->orderBy('menus.id', 'asc')
                ->latest()
                ->get();

            if(count($Menus) > 0) {
                array_push($data, [
                    "id_kategory" => $value->id,
                    "nama_kategory" => $value->name,
                    "data" => $Menus
                ]);               
            }
        }

        if(!$data){
            return $this->sendError('FAILED GET DATA MENU',  $data, 204);    
        }
        return $this->sendSuccess($data, 'SUCCESS GET DATA MENU', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //'id_category', 'name', 'price', 'pic', 'disc', 'markup_gofood', 'markup_grabfood', 'description'

        $this->validate(
            $request,
            [
                'id_category' => 'required',
                'name' => 'required|string',
                'price' => 'required',
            ],
            [
                'id_category.required' => 'Isian Kategory wajib diisi',
                'name.required' => 'Isian Name wajib diisi',
                'price.required' => 'Isian Price wajib diisi',
            ]
        );

        
        $markup_gofood = !empty($request->markup_gofood) ? (int)$request->markup_gofood : 0;
        $markup_grabfood = !empty($request->markup_grabfood) ? (int)$request->markup_grabfood : 0;
        $file_name = "";
        $fileUpload = 'uploads/menu';

        try {  
            
            $add = new Menu();
            $add->id_category = $request->id_category;
            $add->name = $request->name;
            $add->price = $request->price;            
            $add->disc = $request->disc;
            $add->markup_gofood = $markup_gofood;
            $add->markup_grabfood = $markup_grabfood;
            $add->description = $request->description;

            if ($request->hasFile('pic')) {  
                $image = $request->file('pic');        
                $file_name = str_replace(" ","", strtolower(trim($request->name))) ."-". time() . '-' . mt_rand() .".". $image->getClientOriginalExtension();
                $image->move($fileUpload,$file_name);
                $add->pic = env('APP_URL').'/public/uploads/menu/'.$file_name;
            }           
            

            $saveAdd = $add->save();
            
            if(!$saveAdd){
                return $this->sendError('SUCCESS CREATED MENU',  $add, 204);    
            }

            //return response()->json(["message"=>"ok "], 201);
            return $this->sendSuccess($add, 'SUCCESS CREATED MENU', 201);
            

        } catch(\Exception $e) {
            
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //$show = Menu::find($request->id);

        $Menu = DB::table('menus')
            ->leftJoin('menu_categories', 'menus.id_category', '=', 'menu_categories.id')
            ->orderBy('menus.id', 'asc')
            ->select('menus.*', 'menu_categories.name as category_name')
            ->where('menus.id', '=', $request->id)
            ->get();

        if(!$Menu){
            return $this->sendError('SUCCESS SHOW MENU',  $Menu, 204);    
        }
        return $this->sendSuccess($Menu, 'SUCCESS SHOW MENU', 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $this->validate(
            $request,
            [
                'id_category' => 'required',
                'name' => 'required|string',
                'price' => 'required',
            ],
            [
                'id_category.required' => 'Isian Kategory wajib diisi',
                'name.required' => 'Isian Name wajib diisi',
                'price.required' => 'Isian Price wajib diisi',
            ]
        );

        $markup_gofood = !empty($request->markup_gofood) ? (int)$request->markup_gofood : 0;
        $markup_grabfood = !empty($request->markup_grabfood) ? (int)$request->markup_grabfood : 0;
        $file_name = "";
        $fileUpload = 'uploads/menu';        

        try {  

            $update = Menu::find($request->id);    
            
            if(is_null($update)){
                return $this->sendSuccess(NULL, 'NO DATA MENU', 204);
            }

            $update->id_category = $request->id_category;
            $update->name = $request->name;
            $update->price = $request->price;            
            $update->disc = $request->disc;
            $update->markup_gofood = $markup_gofood;
            $update->markup_grabfood = $markup_grabfood;
            $update->description = $request->description;

            if ($request->hasFile('pic')) {  
                $image = $request->file('pic');        
                $file_name = str_replace(" ","", strtolower(trim($request->name))) ."-". time() . '-' . mt_rand() .".". $image->getClientOriginalExtension();
                $image->move($fileUpload,$file_name);
                $update->pic = env('APP_URL').'/public/uploads/menu/'.$file_name;
            }           

            $saveUpdate = $update->save();

            if(!$saveUpdate){
                return $this->sendError('FAILED UPDATE MENU',  $update, 204);    
            } else {
                return $this->sendSuccess($update, 'SUCCESS UPDATE MENU', 200);
            }
            
        } catch(\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {  

            $delete = Menu::find($request->id);
            if(is_null($delete)){
                return $this->sendSuccess(NULL, 'NO DATA MENU', 204);
            }
            $delete->delete();

            if(!$delete){
                return $this->sendError('FAILED DELETE MENU',  $delete, 204);    
            } else {
                return $this->sendSuccess($delete, 'SUCCESS DELETED MENU', 200);
            }
            
        } catch(\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
        

    }
}
