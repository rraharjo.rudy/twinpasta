<?php

use Illuminate\Database\Seeder;

class TableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Table::create([
            'name' => 'Meja 1',
            'description' => 'Description'
        ]);

        \App\Table::create([
            'name' => 'Meja 2',
            'description' => 'Description'
        ]);

        \App\Table::create([
            'name' => 'Meja 3',
            'description' => 'Description'
        ]);

        \App\Table::create([
            'name' => 'Meja 4',
            'description' => 'Description'
        ]);

        \App\Table::create([
            'name' => 'Meja 5',
            'description' => 'Description'
        ]);

        \App\Table::create([
            'name' => 'Meja 6',
            'description' => 'Description'
        ]);

    }
}
