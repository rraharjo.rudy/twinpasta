<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ResponseBaseController;
use App\Table;
use Illuminate\Http\Request;

class TableController extends ResponseBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tables = Table::all();
        return $this->sendSuccess($tables, 'SUCCESS GET TABLES', 200);
    }

    public function index_admin(Request $request)
    {
        $tables = Table::all();
        return $this->sendSuccess($tables, 'SUCCESS GET TABLES', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return response()->json(["req"=>$request->all()]);
        $this->validate(
            $request,
            [
                'name' => 'required|string'
            ],
            [
                'name.required' => 'Isian Name wajib diisi'
            ]
        );

        try {

            $add = new Table();
            $add->name = $request->name;
            $add->description = $request->description;
            $add->save();
            
            if(!$add->save()){
                return $this->sendError('FAILED CREATED TABLE',  $add, 403);    
            }
            return $this->sendSuccess($add, 'SUCCESS CREATED TABLE', 201);

        } catch (\Exception $e){
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $Table = Table::findOrFail($request->id);
        return $this->sendSuccess($Table, "SUCCESS SHOW DATA", 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function edit(Table $table)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'required',
                'name' => 'required|string'
            ],
            [
                'id.required' => 'id null',
                'name.required' => 'Isian Name wajib diisi'
            ]
        );

        try {
            
            $update = Table::findOrFail($request->id);    
            //return $this->sendSuccess($update, 'SUCCESS UPDATE TABLE', 200);
            $update->name = $request->name;
            $update->description = $request->description;
            $update->save();

            if(!$update->save()){
                return $this->sendError('FAILED UPDATE TABLE',  $update, 403);    
            } else {
                return $this->sendSuccess($update, 'SUCCESS UPDATE TABLE', 200);
            }


        } catch (\Exeption $e){
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {  

            $delete = Table::find($request->id);
            
            if(is_null($delete)){
                return $this->sendSuccess(NULL, 'NO DATA TABLE', 204);
            }

            $delete->delete();

            if(!$delete){
                return $this->sendError('FAILED DELETE TABLE',  $delete, 204);    
            } else {
                return $this->sendSuccess($delete, 'SUCCESS DELETED TABLE', 200);
            }
            
        } catch(\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }
}
