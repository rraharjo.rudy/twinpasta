window.Vue = require('vue');

import Vuex from 'vuex'
import auth from './modules/auth'
import table from './modules/table'
import stock from './modules/stock'
import unit from './modules/unit'
import menu from './modules/menu'
import menucategories from './modules/menucategories'
import supportonline from './modules/supportonline'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {},
    getters : {},
    mutations: {},
    actions:{},
    
    modules: {
        namespaced: true,
        auth,
        stock,
        unit,
        table,
        menu,
        menucategories,
        supportonline,
    },
})