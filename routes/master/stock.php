<?php
Route::group(['prefix' => 'admin/stock', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\StockController@index');  
    Route::post('add', 'API\StockController@store')->middleware(['scope:super-admin,admin']);           
    Route::post('update', 'API\StockController@update')->middleware(['scope:super-admin,admin']); 
    Route::post('detail', 'API\StockController@show');         
    // Route::post('delete', 'API\StockController@destroy')->middleware(['scope:super-admin,admin']);      
});