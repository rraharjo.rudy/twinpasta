<?php

namespace App\Http\Controllers\API;

use App\Unit;
use Illuminate\Http\Request;

class UnitController extends ResponseBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Units = Unit::all();
        return $this->sendSuccess($Units, "SUCCESS GET UNITS", 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required|string',
            ],
            [
                'name.required' => 'Isian Name wajib diisi',
            ]
        );

        try {

            $add = new Unit();
            $add->name = $request->name;
            $add->description = $request->description;
            $add->save();

            if(!$add){
                return $this->sendError('SUCCESS CREATED CATEGORY',  $add, 204);    
            }
            
            return $this->sendSuccess($add, 'SUCCESS CREATED CATEGORY', 201);

        } catch(\Exception $e) {
            return $this->sendError("SERVER ERROR", $e->getMessage(), $e->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'required',
                'name' => 'required|string',
            ],
            [
                'id.required' => 'ID NULL',
                'name.required' => 'Isian Name wajib diisi',
            ]
        );

        try {

            $update = Unit::find($request->id);    
            
            if(is_null($update)){
                return $this->sendSuccess(NULL, 'NO DATA UNIT', 204);
            }

            $update->name = $request->name;
            $update->save();

            if(!$update){
                return $this->sendError('FAILED UPDATE UNIT',  $update, 204);    
            } else {
                return $this->sendSuccess($update, 'SUCCESS UPDATE UNIT', 200);
            }            

        } catch (\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {  

            $delete = Unit::find($request->id);

            if(is_null($delete)){
                throw new Exception();
            }
            $delete->delete();

            if(!$delete){
                return $this->sendError('FAILED DELETE UNIT',  $delete, 204);    
            } else {
                return $this->sendSuccess($delete, 'SUCCESS DELETED UNIT', 200);
            }
            
        } catch(\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), $e->getCode());
        }
    }
}
