<?php
Route::group(['prefix' => 'admin/menu-category', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\MenuCategoryController@index');          
    Route::post('add', 'API\MenuCategoryController@store')->middleware(['scope:super-admin,admin']);           
    Route::post('update', 'API\MenuCategoryController@update')->middleware(['scope:super-admin,admin']);    
    Route::post('delete', 'API\MenuCategoryController@destroy')->middleware(['scope:super-admin,admin']);                  
});