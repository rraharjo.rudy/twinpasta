<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FoodSupportOnline extends Model
{
    use SoftDeletes;

    protected $table = 'food_support_onlines';

    protected $dates = ['deleted_at'];
}
