// State
const state = {
    listtable: [],
    // totalItems: 0,
    // pagination: {
    //     current_page: 0,
    //     rowsPerPage: 0,
    //     lastpage:0
    // }
}

// getters
const getters = {
    GET_DATA(state){
        return state.listtable !== null
    }
}

// mutations
const mutations = {
    MUT_GET(state, payload) {
        state.listtable = payload.result
    },
    MUT_ADD(state, payload){
        state.listtable.push(payload.result)
    },
    MUT_UPDATE(state, payload){
        const idx = state.listtable.map(t => t.id).indexOf(payload.result.id)
        state.listtable.splice(idx, 1, payload.result)
        // console.log(idx, payload.result.id) // eslint-disable-line
    },
    MUT_DELETE(state, payload){
        //console.log(payload)
        state.listtable.splice(payload, 1)
    }
}

// actions
const actions = {

    ACT_LOADTABLES (context, credentials) {  

        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.get(credentials)
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_GET', res.data)
                    resolve(res.data.result)
                } else {
                    reject('No Data')
                }
            })
            .catch(err => {
              reject(err)
            })
        });
    },
    ACT_SAVE(context, credentials) {
        
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.post('/admin/table/add', {
                name: credentials.name,
                description: credentials.description,
            })
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_ADD', res.data)
                    resolve(res.data.result)
                } else {
                    reject('Failed add table . Please Form Field')
                }
            })
            .catch(err => {
                reject(err)
            })
        });
    },
    ACT_UPDATE(context, credentials) {
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.post('/admin/table/update', {
                id:credentials.id,
                name: credentials.name,
                description: credentials.description,
            })
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_UPDATE', res.data)
                    resolve(res.data.result)
                } else {
                    reject('Failed add table . Please Form Field')
                }
            })
            .catch(err => {
                reject(err)
            })
        });
    },
    ACT_DELETED(context, credentials) {
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.post('/admin/table/delete', {
                id:credentials.id
            })
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_DELETE', credentials.index)
                    resolve(res.data)
                } else {
                    reject('Failed delete table')
                }
            })
            .catch(err => {
                reject(err)
            })
        });

    }

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}