<?php
Route::group(['prefix' => 'support-ojol', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\FoodSupportOnlineController@index');          
    Route::post('add', 'API\FoodSupportOnlineController@store')->middleware(['scope:super-admin,admin']);           
    Route::post('update', 'API\FoodSupportOnlineController@update')->middleware(['scope:super-admin,admin']);    
    Route::post('delete', 'API\FoodSupportOnlineController@destroy')->middleware(['scope:super-admin,admin']);                  
});