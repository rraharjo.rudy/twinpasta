<?php
use Illuminate\Http\Request;

// MASTER ROUTES
include __DIR__.'/auth/auth.php';
include __DIR__.'/master/supplier.php';
include __DIR__.'/master/table.php';
include __DIR__.'/master/stock.php';
include __DIR__.'/master/menu.php';
// END MASTER ROUTES

// CONFIG ROUTES
include __DIR__.'/config/menu-category.php';
include __DIR__.'/config/unit.php';
include __DIR__.'/config/support-ojol.php';
// END CONFIG ROUTES


// TEST SCOPE
Route::get('/super-admin', function (Request $request) {
    return response()->json(["data"=>"only admin"]);
})->middleware(['auth:api', 'scope:super-admin']);

Route::get('/waiter', function (Request $request) {
    return response()->json(["data"=>"waiter"]);
})->middleware(['auth:api', 'scope:super-admin,waiter']);

// END TEST SCOPE