// State
const state = {
    listsupportonlines: [],
}

// getters
const getters = {
    GET_DATA(state){
        return state.listsupportonlines !== null
    }
}

// mutations
const mutations = {
    MUT_GET(state, payload) {
        state.listsupportonlines = payload
    },
}

// actions
const actions = {

    ACT_LOADSUPPORTONLINES(context, credentials) {  
              
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.get(credentials)
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_GET', res.data.result)
                    //resolve(res.data.result)
                } else {
                    reject('No Data')
                }
            })
            .catch(err => {
                reject(err)
            })
        });
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}