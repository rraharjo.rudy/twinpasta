<?php
Route::group(['prefix' => 'table', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\TableController@index');                      
});
Route::group(['prefix' => 'admin/table', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\TableController@index_admin');  
    Route::post('add', 'API\TableController@store')->middleware(['scope:super-admin,admin']);           
    Route::post('detail', 'API\TableController@show');         
    Route::post('update', 'API\TableController@update')->middleware(['scope:super-admin,admin']); 
    Route::post('delete', 'API\TableController@destroy')->middleware(['scope:super-admin,admin']);      
});