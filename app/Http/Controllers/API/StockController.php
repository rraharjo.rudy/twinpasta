<?php

namespace App\Http\Controllers\API;


use App\Stock;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StockController extends ResponseBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$liststock = Stock::paginate(10);
        
        $stocks = DB::table('stocks')
            ->leftJoin('units', 'stocks.id_unit', '=', 'units.id')
            ->orderBy('stocks.id', 'asc')
            ->select('stocks.*', 'units.name as name_unit')
            ->get();
            // ->paginate(10);

        if(!$stocks){
            return $this->sendError('FAILED GET DATA STOCK',  $stocks, 204);    
        }
        return $this->sendSuccess($stocks, 'SUCCESS GET DATA STOCK', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // 'code_stock', 'id_unit', 'name', 'pic', 'price', 'qty', 'qty_min'
        $this->validate(
            $request,
            [
                'id_unit' => 'required',
                'name' => 'required',
                'price' => 'required',
                'qty' => 'required',
                'qty_min' => 'required',
            ],
            [
                'id_unit.required' => 'Isian Unit  wajib diisi',
                'name.required' => 'Isian Nama wajib diisi',
                'price.required' => 'Isian Harga wajib diisi',
                'qty.required' => 'Isian Qty wajib diisi',
                'qty_min.required' => 'Isian Minimal Qty wajib diisi',
            ]
        );
        
        try {

            $add = new Stock();

            $add->code_stock = '-';
            $add->id_unit = $request->id_unit;
            $add->name = $request->name;
            $add->price = $request->price;
            $add->qty = $request->qty;
            $add->qty_min = $request->qty_min;
            $add->save();
            $add->code_stock = 'CDSTK-' . sprintf('%06d', $add->id);
            $add->update();

            if(!$add){
                return $this->sendError('SUCCESS CREATED MENU',  $add, 204);    
            }
            return $this->sendSuccess($add, 'SUCCESS ADD STOCK', 201);

        } catch (\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $stock = DB::table('stocks')
            ->leftJoin('units', 'stocks.id_unit', '=', 'units.id')
            ->orderBy('stocks.id', 'asc')
            ->select('stocks.*', 'units.name as unit_name')
            ->where('stocks.id', '=', $request->id)
            ->get();

        //$show = Stock::find($request->id);
        if(!$stock){
            return $this->sendError('FAILED SHOW STOCK',  $stock, 204);    
        } else {
            return $this->sendSuccess($stock, 'SUCCESS SHOW STOCK', 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        $this->validate(
            $request,
            [
                'id' => 'required',
                'id_unit' => 'required',
                'name' => 'required',
                'price' => 'required',
                'qty' => 'required',
                'qty_min' => 'required',
            ],
            [
                'id.required' => 'ID Stock NULL',
                'id_unit.required' => 'Isian Unit  wajib diisi',
                'name.required' => 'Isian Nama wajib diisi',
                'price.required' => 'Isian Harga wajib diisi',
                'qty.required' => 'Isian Qty wajib diisi',
                'qty_min.required' => 'Isian Minimal Qty wajib diisi',
            ]
        );
        $data = array();
        try {

            $update = Stock::find($request->id);

            if(is_null($update)){
                return $this->sendSuccess(NULL, 'NO DATA STOCKS', 200);
            }

            $update->id_unit = $request->id_unit;
            $update->name = $request->name;
            $update->price = $request->price;
            $update->qty = $request->qty;
            $update->qty_min = $request->qty_min;
            $update->save();


            if(!$update){
                return $this->sendError('FAILED UPDATE STOCKS',  $stock, 204);    
            } else {
                
                $stock = DB::table('stocks')
                    ->leftJoin('units', 'stocks.id_unit', '=', 'units.id')
                    ->orderBy('stocks.id', 'asc')
                    ->select('stocks.*', 'units.name as name_unit')
                    ->where('stocks.id', '=', $request->id)
                    ->get();

                return $this->sendSuccess($stock[0], 'SUCCESS UPDATE STOCKS', 200);
            }            

        } catch (\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {  

            $delete = Stock::find($request->id);
            
            if(is_null($delete)){
                return $this->sendSuccess(NULL, 'NO DATA STOCK', 204);
            }

            $delete->delete();

            if(!$delete){
                return $this->sendError('FAILED DELETE STOCK',  $delete, 204);    
            } else {
                return $this->sendSuccess($delete, 'SUCCESS DELETED STOCK', 200);
            }
            
        } catch(\Exception $e) {
            return $this->sendError('SERVER ERROR.',  $e->getMessage(), 500);
        }
    }
}
