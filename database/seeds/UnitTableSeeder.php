<?php

use Illuminate\Database\Seeder;

class UnitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Unit::create([
            'name' => 'pcs',
            'description' => 'pieces'
        ]);

        \App\Unit::create([
            'name' => 'lusin',
            'description' => 'lusin'
        ]);
    }
}
