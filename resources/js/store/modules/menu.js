// State
const state = {
    listmenus: [],
}

// getters
const getters = {
    GET_DATA(state){
        return state.listmenus !== null
    }
}

// mutations
const mutations = {
    MUT_GET(state, payload) {
        state.listmenus = payload
    },
    MUT_ADD(state, payload){
        state.listmenus.push(payload.result)
    },
    MUT_UPDATE(state, payload){
        const idx = state.listmenus.map(t => t.id).indexOf(payload.result.id)
        state.listmenus.splice(idx, 1, payload.result)
    },
    MUT_DELETE(state, payload){
        state.listmenus.splice(payload, 1)
    }
}

// actions
const actions = {

    ACT_LOADMENUS(context, credentials) {  
              
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.get(credentials)
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_GET', res.data.result)
                    resolve(res.data.result)
                } else {
                    reject('No Data')
                }
            })
            .catch(err => {
                reject(err)
            })
        });
    },
    ACT_SAVE(context, credentials) {
        
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        console.log(credentials)
        return new Promise((resolve, reject) => {
            axios.post('/admin/menu/add', credentials,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_ADD', res.data)
                    resolve(res.data.result)
                } else {
                    //console.log(res.data.error)
                    reject('Failed add table . Please Form Field')
                }
            })
            .catch(err => {
                console.log(err)
                reject(err)
            })
        });
    },
    ACT_UPDATE(context, credentials) {
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.post('/admin/menu/update', credentials,{
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_UPDATE', res.data)
                    resolve(res.data.result)
                } else {
                    console.log(res.data.error)
                    reject('Failed update data . Please Check Form Field')
                }
            })
            .catch(err => {
                reject(err)
            })
        });
    },
    ACT_DELETED(context, credentials) {
        const Type = JSON.parse(localStorage.getItem('ses_storage')).token.token_type
        const Token = JSON.parse(localStorage.getItem('ses_storage')).token.access_token
        axios.defaults.headers.common['Authorization'] = Type +' '+ Token

        return new Promise((resolve, reject) => {
            axios.post('/admin/menu/delete', {
                id:credentials.id
            })
            .then(res => {
                if(res.data.success){
                    context.commit('MUT_DELETE', credentials.index)
                    resolve(res.data)
                } else {
                    reject('Failed delete table')
                }
            })
            .catch(err => {
                reject(err)
            })
        });

    }

}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}