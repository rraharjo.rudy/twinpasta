<?php

use Illuminate\Database\Seeder;

class MenuCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\MenuCategory::create([
            'name' => 'Makanan',
        ]);

        \App\MenuCategory::create([
            'name' => 'Minuman',
        ]);
        
        \App\MenuCategory::create([
            'name' => 'Jajanan Bocah',
        ]);
    }
}
