<?php
Route::group(['prefix' => 'admin/unit', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\UnitController@index');          
    Route::post('add', 'API\UnitController@store')->middleware(['scope:super-admin,admin']);           
    Route::post('update', 'API\UnitController@update')->middleware(['scope:super-admin,admin']);    
    Route::post('delete', 'API\UnitController@destroy')->middleware(['scope:super-admin,admin']);                  
});