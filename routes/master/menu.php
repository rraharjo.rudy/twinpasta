<?php
Route::group(['prefix' => 'menu', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\MenuController@index');          
});

Route::group(['prefix' => 'admin/menu', 'middleware' => 'auth:api'], function () {
    Route::get('list', 'API\MenuController@index_admin');          
    Route::post('detail', 'API\MenuController@show');         
    Route::post('add', 'API\MenuController@store')->middleware(['scope:super-admin,admin']);           
    Route::post('update', 'API\MenuController@update')->middleware(['scope:super-admin,admin']);           
    Route::post('delete', 'API\MenuController@destroy')->middleware(['scope:super-admin,admin']);           
});

