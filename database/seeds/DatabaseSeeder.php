<?php

use App\FoodSupportOnline;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            SupplierTableSeeder::class,
            TableTableSeeder::class,
            FoodSupportOnlineSeeder::class,
            RolesAndPermissionsSeeder::class,
            MenuCategoriesSeeder::class,
            UnitTableSeeder::class,
            UsersTableSeeder::class,
            MenusTableSeeder::class,
            StockTableSeeder::class,
        ]);
    }
}
