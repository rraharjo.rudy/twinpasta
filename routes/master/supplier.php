<?php
Route::group(['prefix' => 'supplier', 'middleware' => 'auth:api'], function () { 
    Route::get('list', 'API\SupplierController@index');  
    Route::post('add', 'API\SupplierController@store')->middleware(['scope:super-admin,admin']);           
    Route::post('detail', 'API\SupplierController@show');         
    Route::post('update', 'API\SupplierController@update')->middleware(['scope:super-admin,admin']); 
    Route::post('delete', 'API\SupplierController@destroy')->middleware(['scope:super-admin,admin']);                            
});