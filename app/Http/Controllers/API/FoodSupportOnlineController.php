<?php

namespace App\Http\Controllers\API;

use App\FoodSupportOnline;
use Illuminate\Http\Request;

class FoodSupportOnlineController extends ResponseBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SupportOnline = FoodSupportOnline::all();
        //$SupportOnline = FoodSupportOnline::onlyTrashed()->get();
        return $this->sendSuccess($SupportOnline, 'SUCCESS GET DATA', 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request,
            [
                'name' => 'required|string',
            ],
            [
                'name.required' => 'Isian Name wajib diisi',
            ]
        );

        try {

            $add = new FoodSupportOnline();
            $add->name = $request->name;
            $add->price = $request->price;
            $add->persentase = $request->persentase;
            $addSave = $add->save();
            
            if(!$addSave){
                return $this->sendError('SUCCESS CREATED SUPPORT OJOL',  $add, 204);    
            }
            
            return $this->sendSuccess($add, 'SUCCESS CREATED SUPPORT OJOL', 201);

        } catch (\Exception $e) {
            return $this->sendError('SOMETHING WRONG.',  $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodSupportOnline  $foodSupportOnline
     * @return \Illuminate\Http\Response
     */
    public function show(FoodSupportOnline $foodSupportOnline)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodSupportOnline  $foodSupportOnline
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodSupportOnline $foodSupportOnline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodSupportOnline  $foodSupportOnline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'required',
                'name' => 'required|string',
            ],
            [
                'id.required' => 'ID NULL',
                'name.required' => 'Isian Name wajib diisi',
            ]
        );

        try {

            $update = FoodSupportOnline::find($request->id);

            if(is_null($update)){
                return $this->sendSuccess(NULL, 'NO DATA', 204);
            }

            $update->name = $request->name;
            $update->price = $request->price;
            $update->persentase = $request->persentase;
            $updateSave = $update->save();

            if(!$updateSave){
                return $this->sendError('FAILED UPDATE',  $update, 204);    
            } else {
                return $this->sendSuccess($update, 'SUCCESS UPDATE', 200);
            }          

        } catch (\Exception $e) {
            return $this->sendError('SOMETHING WRONG.',  $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodSupportOnline  $foodSupportOnline
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->validate(
            $request,
            [
                'id' => 'required',
            ],
            [
                'id.required' => 'ID NULL',
            ]
        );

        try {

            $delete = FoodSupportOnline::find($request->id);
            if(is_null($delete)){
                return $this->sendSuccess(NULL, 'NO DATA', 204);
            }
            $deleteSave = $delete->delete();

            if(!$deleteSave){
                return $this->sendError('FAILED DELETE',  $delete, 204);    
            }
                
            return $this->sendSuccess($delete, 'SUCCESS DELETED', 200);
            

        } catch (\Exception $e) {
            return $this->sendError('SOMETHING WRONG.',  $e->getMessage(), 500);
        }
    }
}
