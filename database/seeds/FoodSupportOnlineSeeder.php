<?php

use Illuminate\Database\Seeder;

class FoodSupportOnlineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\FoodSupportOnline::create([
            'name' => 'Go Jek',
            'price' => '2500',
            'persentase' => 0
        ]);

        \App\FoodSupportOnline::create([
            'name' => 'Grab Bike',
            'price' => '2500',
            'persentase' => 0
        ]);
    }
}
