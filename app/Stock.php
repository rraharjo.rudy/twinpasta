<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use SoftDeletes;

    protected $table = "stocks";

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'code_stock', 'id_unit', 'name', 'pic', 'price', 'qty', 'qty_min'
    ];

    public function unit()
	{
		return $this->hasOne('App\Unit', 'id');
    }
}
